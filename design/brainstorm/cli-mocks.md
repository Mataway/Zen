# Zen CLI mockups

These are to give an idea of what basic Zen command line usage might look/feel like, and validate that the terminology/syntax for different commands and entities feel cohesive in normal usage.

## Initializing a Zen instance from scratch

    % cd Ace
    % ls -l
    total 16
    -rw-r--r--  1 samwise  staff  12 Feb  1 22:23 bar
    -rw-r--r--  1 samwise  staff  40 Jun 11  2017 foo
    
    % zn init
    //Ace depot ready to go!  Run 'zn add ...' and 'zn commit' to add your first files.
    
    % zn add ...
    bar - opened for add
    foo - opened for add
    
    % zn commit
    Local change 1 created with 2 open file(s).
    Committing change 1 to stream main...
    bar - added
    foo - added
    Change 1 committed.

## Local branching and editing

    % zn switch -c dev
    Switched to new stream dev.
    
    % vi foo
    
    % zn commit -a
    Local change 2 created with 1 open file(s).
    Committing change 2 to stream //Ace/dev...
    foo - edited
    Change 2 committed.
    
    % zn switch main
    Switched to stream main.
    
    % zn merge --from dev --auto
    foo - copy from //Ace/dev/foo@2
    1 file merged to main from dev.
    
    % zn commit
    Local change 3 created with 1 open file(s).
    Committing change 3 to stream //Ace/main/...
    foo - edited
    Change 3 committed.

## Seeding the nucleus

    % zn connect nucleus:1666
    Connected to nucleus at nucleus.zenofthemonkey.com:1666
    No global depots or streams have been created yet.  Run 'zn push' to push //Ace/main.
    
    % zn push
    Converting //Ace/main from local to global...
    2 files with 3 revisions shared, no changes renumbered.
    //Ace/main is now globally shared.

## A new person joins the team

    % cd Ace
    
    % zn clone zn://nucleus:1666/Ace
    Cloning //Ace/main...
    //Ace/main has been added to your streams and synced to your workspace.
    
    % vi bar
    
    % zn commit -a
    Shared change 4 created with 1 open file(s).
    Committing change 4 to stream //Ace/main/...
    bar - edited
    Change 4 committed.