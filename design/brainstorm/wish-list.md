This is an assortment of random requests and ideas that have come up in conversation:

* native support for image macros in changelist descriptions
* directory versioning
* docs written in asciidoctor; rendered to CLI or web as needed
* user names should be changeable after the fact; people should be able to change their real life names if they want and not be haunted by their old name in commits