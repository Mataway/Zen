## CLI Design Guidelines

When looking to add commands the following design guidelines should be considered:

* Commands should have long flag names and short names for common flags.
* Commands should use the same flag to mean the same thing; i.e. -s should
always mean stream.
* Highly destructive commands should have long awkward names.
* Commands that are common should have sensible short versions (everyone in the
world should not have to alias checkout to co.)
* Command output should be for humans; scripts can request formatted data.
* Command output can be more than 80 chars; it's not 1970. Shorter is better 
though.
* When in doubt language should be more git-like than p4-like; there are way
the hell more git users in the world.
* Not everyone will be happy with whatever we come up with; that's why god gave
us aliases.
* Commands should have reasonable defaults if no argument is specified; 
if I run `zn stream` while sitting in a stream, then maybe show me my current
 one?
* Try not to invent new terms; developers don't need more fancy new words. 
We have plenty of good words.
* Try not to have 6 things are all practically the same thing. Base 
configuration should be usable, then you take advantage of more fields if 
you need them.
* Inspirations: Kubernetes, Git, Hg, Go

## Commands
### Must Have
* zn help - get help
* zn info - info about connections to global servers, version info, kind of a 
bucket of interesting configuration data useful for debugging/support
* zn init/initialize - create a new Zen depot
* zn ignore - add files to the ignore file
* zn commit/submit/ci - commit changes
* zn status - list out files that have changed or need to be added
* zn stream - view and edit a stream specification
* zn switch - move between streams in a depot
* zn depot  - view and edit a depot specification

### Nice to Have
* zn alias - manage aliases
* zn shelve - cold storage for changes (https://blogs.msdn.microsoft.com/oldnewthing/20180122-00/?p=97855)
* zn where - show which depot a file comes from, and how it is mapped into your depot
