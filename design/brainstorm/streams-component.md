# Zen streams -- component based development

A stream may comprise multiple other streams by _importing_ them, such that the contents of the imported stream are mapped directly into a path within the importing stream.

A stream's parent and child streams always reside within the same depot, and represent different versions of the same component.  Imported streams always reside within a different depot from the importing stream, since they represent specific versions of entirely different components.

## Imports in the stream spec

A stream's imports are defined in the stream spec, similarly to its Paths.

### Import syntax

    Import:
        //Elm/rel1/... for read as Elm/...
        //Oak/main/... for write as Oak/...
        //Willow/main/...@v1 for read as Willow-v1/...

An import line contains the following elements:

* The source of the import (another stream or a path within it)
* The type of the import (read-only or writable)
* The target of the import (a path within the importing stream)

If the source includes a revision specifier, it may only be imported as read-only.  If an import is writable, changes may be committed directly to the imported stream.

### Propagation of imports

Unlike Paths, which are implicitly modified by the parent, imports are branched from the parent when a stream is first created, and may be modified completely independently.  Changes to imports are propagated during merge operations between related streams.

Here is an example of this workflow:

    % zn stream -o
    Stream: //Forest/main
    Paths:
        public ...
        private bin/...
    Imports:
        //Oak/rel1/... for read as Oak/...
    
    % zn switch -c dev
    Switched to new stream dev.
    
    % zn stream -o
    Stream: //Forest/dev
    Paths:
        public ...
    Imports:
        //Oak/rel1/... for read as Oak/...

    % zn stream
    (the Imports line is changed to import //Oak/rel2 instead of rel1)
    Imports:
        //Oak/rel2/... for read as Oak/...
    Stream dev saved.
    Imports modified, run 'zn sync' to update your workspace.
    
    % zn sync
    Oak/leaf.c - updated //Oak/rel2/leaf.c@1013
    Oak/leaf.h - updated //Oak/rel2/leaf.h@1011

    % zn commit
    Global change 1223 created with 0 open file(s).
    Committing change 1223 to stream //Forest/dev...
    //Forest/dev - imports updated.
    Change 1223 committed.
    
    % zn switch main
    Switched to stream main.
    
    % zn merge --from dev
    //Forest/main - copy imports from //Forest/dev
    Imports modified, run 'zn sync' to update your workspace.
    
    % zn stream -o
    Stream: //Forest/main
    Paths:
        public ...
        private bin/...
    Imports:
        //Oak/rel2/... for read as Oak/...
    
    % zn sync
    Oak/leaf.c - updated //Oak/rel2/leaf.c@1013
    Oak/leaf.h - updated //Oak/rel2/leaf.h@1011
    