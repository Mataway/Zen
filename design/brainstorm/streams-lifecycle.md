# Zen streams -- lifecycle modeling

The most fundamental use of streams in Zen is to manage the flow of change between different stages of your code's lifecycle -- from squishy early development to the central mainline to the firm release branch.

## Stream parents

Each stream has a "parent" that changes flow to and from, and may in turn have multiple children.  A Zen depot starts with a "main" stream that has no parent, from which other streams are created.

## Stream path types

Different sets of the files in a stream can be given different policies with respect to how those files are shared with the stream's parent and/or children.  These policies are applied to directories within a stream.

### public

A _public_ path contains files that are:

 * owned and modifiable by this stream
 * branched from its parent (if permitted)
 * branchable by its children

This is the policy for all files in a stream by default.

### virtual

A _virtual_ path contains files that are:

 * modifiable but not owned by this stream
 * mapped directly from its parent
 * mapped through to its children

A _virtual_ path is used to create a child stream that represents a limited subset of its parent rather than a branched variant.  Commits made to the _virtual_ path go directly to the parent stream.

### private

A _private_ path contains files that are:

 * owned and modifiable by this stream
 * not branched from its parent
 * not branched to its children

Files that are peculiar to a particular codeline, whose changes should not flow to other codelines (e.g. files that are very release-specific or not mergeable) may be specified as _private_ to prevent them from being merged.

### inherit

An _inherit_ path contains files that are:

 * not owned or modifiable by this stream
 * inherited directly from its parent
 * inherited directly by its children

A path that is _inherited_ acts like a read-only symlink to the corresponding parent path.  When files in this path are synced, they will come directly from the parent.  Changes may not be submitted to these files.  If a child stream defines a path that would normally branch inherited files, the files will instead be inherited.

_Inherit_ paths may specify a particular version of the parent path to be inherited.  Newer changes committed to the parent path will not appear in the inherited path in the child.

## Path combinations

When a parent and child stream declare different policies for the same path, the less permissive policy is used -- hence child streams may downgrade permissions but never upgrade them.  A child stream that declares all of its paths to be _public_ will conform to its parent's policies.
	
	Parent path      Child path     Result     Flow of change
	---------------------------------------------------------
	public           public         public     parent<->child on merge
	public           virtual        virtual    parent<->child on sync/commit
	public           private        private    none
	public           inherit        inherit    parent->child on sync
	virtual          public         virtual    parent<->child on sync/commit
	virtual          virtual        virtual    parent<->child on sync/commit
	virtual          private        private    none
	virtual          inherit        inherit    parent->child on sync
	private          public         private    none
	private          virtual        private    none
	private          private        private    none
	private          inherit        inherit    parent->child on sync
	inherit          public         inherit    parent->child on sync
	inherit          virtual        inherit    parent->child on sync
	inherit          private        inherit    parent->child on sync
	inherit          inherit        inherit    parent->child on sync

Notes:

* When a child path inherits a path that is itself inherited, it simply receives the same inherited files as its parent.
* A child may specify an earlier version on an _inherit_ path than its parent does, but not a later one.
* A _private_ path may be inherited by other streams, but never modified by them.
