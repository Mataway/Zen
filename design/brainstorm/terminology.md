Here lies current thoughts on what things in Zen should be named.

* depot - project container containing the system of streams that defines a related
body of work. Similar to a repo in a git management system such as GitLab.

* stream - a variant of a given body of work. Similar to a branch in other version
control systems. As famously said by Laura Wingerd, a stream is a branch with brains.

* team - an organizational unit for grouping related depots. Teams make it easier for
admins to apply access rules.

* org - an organizational unit for grouping teams. Orgs makes it easier for admins
to apply access rules.

* global server - the server that users publish their changes to. Similar to
a repo hosted in a git management system.

* shard - the local instance of Zen that users interact with for most operations

== Common flags across commands
It's always nice when a flag means the same thing no matter which command you use it with. Here lies the naming conventions to use for new commands.

* -s --stream: for pasing a stream arg
* -c --create: for creating new things