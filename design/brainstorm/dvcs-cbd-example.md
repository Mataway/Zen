# A Zen DVCS CBD walkthrough

In this example we will see how Zen's ability to define components and create local branches work together to easily allow distributed work on a very large project.

## Defining the component hierarchy

The top-level component we're working on is called `Mountain`.  `Mountain` has subcomponents `Oak` and `Rock`, and `Oak` has subcomponents `Leaf` and `Root`.  The streams are defined as follows:

	Stream: //Mountain/main
	Scope: global
	Paths:
		public ...
	Import:
		//Oak/main/... for write as Oak/...
		//Rock/main/... for write as Rock/...

	Stream: //Rock/main
	Scope: global
	Paths:
		public ...

	Stream: //Oak/main
	Scope: global
	Paths:
		public ...
	Import:
		//Leaf/main/... for write as Leaf/...
		//Root/main/... for write as Root/...

	Stream: //Leaf/main
	Scope: global
	Paths:
		public ...

	Stream: //Root/main
	Scope: global
	Paths:
		public ...

The filesystem looks like this:

	Mountain/
		Oak/
			Leaf/
			Root/
		Rock/

Since the components are all imported `for write`, commits may be made within these paths, and they will directly affect the imported stream(s).

The `zn where` command displays where a particular file in your current stream came from:

	% zn where Oak/Leaf/foo.c
	//Mountain/main/Oak/Leaf/foo.c
	... imported from //Oak/main/Leaf/foo.c
	... imported from //Leaf/main/foo.c

All of these paths reference the same file as it exists in different stream hierarchies; a commit made to the file from any of these streams will affect all of the others.

## Initializing your local shard

	% zn clone zn://computer:1951/Mountain
	Cloning //Mountain/main...
	//Mountain/main has been added to your streams and synced to your workspace.

You now have the entire `Mountain` project in your workspace, and your local shard is connected to the global `//Mountain/main` stream.  Any commits you make to these files will go directly to this stream in the global server and will be available to every other user who is connected to that stream.

## Think globally, fork locally

The specific part of the Mountain project we plan to work on is the `Leaf` component, and we like to be able to work locally for a while before committing our work to the global stream.  To do this we use the `zn fork` command:

	% zn fork --scope local --stream dev //Leaf
	Forking //Leaf/dev from //Leaf/main@1001...
	Forking //Oak/dev from //Oak/main@1001...
	Forking //Mountain/dev from //Mountain/main@1001...
	Switched to new stream dev.

The `fork` command is used when we want to create a new stream of a particular component in order to isolate our changes.  This is similar to the `zn switch -c` command except that it operates on a particular component stream (`//Leaf/main` in this case) rather than the current stream (`//Mountain/main`), and the importing stream is forked as well since we need a stream that imports the newly forked component stream -- and its importing stream is forked, et cetera, until we reach the current stream.

Since we specified `--scope local` the newly created streams are all local streams.  Their parents are global streams, but references to them are bound to a static changelist (the latest changelist as of the time we ran the `fork` command).

Here are the resulting local streams:

	Stream: //Leaf/dev
	Scope: local
	Parent: //Leaf/main
	Paths:
		public ...

	Stream: //Oak/dev
	Scope: local
	Parent: //Oak/main
	Paths:
		inherit ...@1001
	Import:
		//Leaf/dev/... for write as Leaf/...
		//Root/main/...@1001 for read as Root/...
	
	Stream: //Mountain/dev
	Scope: local
	Parent: //Mountain/main
	Paths:
		inherit ...@1001
	Import:
		//Oak/dev/... for write as Oak/...
		//Rock/main/...@1001 for read as Rock/...

The filesystem is exactly the same as it was before the `fork` command, but now commits made to the `Mountain/Oak/Leaf/...` path will go into the local stream `//Leaf/dev`.  Other paths are read-only by default since we specifically forked the `//Leaf` component on its own, but we are free to make any further changes we like to our local streams.

## Working in our local stream

Files within the `Oak/Leaf` path in our local `//Mountain/dev` stream map to our local `//Leaf/dev` stream:

	% vi Oak/Leaf/foo.c
	% zn commit -a
	Local change 1002 created with 1 open file.
	Committing change 1002 to stream //Mountain/dev/...
	Oak/Leaf/foo.c - edited //Leaf/dev/foo.c
	Change 1002 committed.

At this point no global streams have been affected, because this file is in the stream that we forked locally:

	% zn where Oak/Leaf/foo.c
	//Mountain/dev/Oak/Leaf/foo.c
	... imported from //Oak/dev/Leaf/foo.c
	... imported from //Leaf/dev/foo.c

If we looked at one of the other components, like `Root`, we would see that its files come from a static (locally cached) version of a `main` global stream:

	% zn where Oak/Root/bar.c
	//Mountain/dev/Oak/Root/bar.c
	... imported from //Oak/dev/Root/bar.c
	... imported from //Root/main/bar.c@1001

## Returning to the global stream

When we are ready to share our work with other users of the global stream, we return to that stream and merge our work into it:

	% zn switch main
	Switched to stream main.

	% zn merge --from dev
	Oak/Leaf/foo.c - copy from //Leaf/dev/foo.c
	1 file merged to main from dev.

	% zn commit
	Global change 1003 created with 1 open file.
	Committing change 1003 to stream //Mountain/main/...
	Oak/Leaf/foo.c - edited //Leaf/main/foo.c
	Change 1003 committed.
