# Zen streams -- distributed development

Distributed development in Zen is managed by merging changes between _local_ streams and _global_ streams.  

* A _local_ stream exists only on your local shard.
	* You can create any number of local streams without impacting other users.
	* A network connection is not required to create or commit to local streams.
	* Since you have complete control over your local shard, you may modify local stream history.
* A _global_ stream is synced with the global server.
	* You must be connected to the server in order to commit to a global stream.
	* Global commits are instantly available to all other users.
	* You may need to resolve against other users' changes when making a global commit.

## Creating a global stream

### Pushing a local stream

A local stream may be converted to a global stream by pushing it to the server.  Pushing a stream requires that:

* The local stream's parent (if any) is already global.
* A global stream with that name does not exist yet.

Once a stream has been pushed it is permanently converted into a global stream.  All changelists in the stream are renumbered and appended to the global server's changelist sequence.

### Creating a child of an existing global stream

A stream created as a child of an existing global stream may be specified as _global_ itself.

## Working with global streams

### Adding global streams to your local shard

The `zn fetch` command adds a global stream to your local shard.  Once a global stream has been fetched, you can `zn switch` to it and do work in it much the same way you would work in a local stream.

### Flow of change between differently scoped streams

Propagating a change from a local stream to a global stream uses the same workflow as merging between local streams.  The only difference is that the commit will go directly to the global server and become part of its immutable history.