## Introduction
I refuse to believe that Git is the best we can do for version control in the
21st century. There are many wonderful aspects of Git and it's hard to argue with
its success, but the internet is full of stories of creators fighting with Git and
losing and it has many issues handling larger codebases and files.

Version control tools at their best give you all the information you need
to get your job done effectively and piece of mind so you can focus on the 
problems you get paid for. They also scale to handle your project no matter how
big it may get. Zen will be that tool.

## Design Goals
Zen will be:
* Distributed - the world has spoken and distributed versioning is a requirement.
* Monorepo friendly - many people love them a monorepo, but current open source tools 
don't support them well. Zen will be monorepo focused from the start.
* User focused - shockingly enough most people don't want to think much about their 
version control system. Zen will be thoughtful about user needs and minimize arcane 
workflows (git rebase), overloaded behavior in a single command (git checkout) and 
weird inputs (Perforce client views.)
* Open source - developers and companies cannot be held hostage by their tools. We 
all invest too much money and time in our toolchains to potentially have them thrown 
into chaos by a tool provider being acquired or closed, or having a tool deprecated 
that we depend on. Zen will be open source from the start.
* Functional with large files - game dev is best when art and code can be committed 
together in one repo. Big file support is a must.
* Stream oriented - Streams help make a version control system user focused by making 
it easy for them to figure out where their change flows.
* Component based development oriented - a monorepo focused version control system 
should make it easy to bring together parts of the repo in a logical way at known 
valid points. We can work around this by using external systems, but the version 
control system itself is best positioned to help coordinate changes across multiple
systems, especially in the era of microservices.

## The Plan
To the extent there is any plan to take this thing from a zenifesto to working
software here it is. I'm sure this will change dramatically as time goes on; thankfully
this is all versioned so we can laugh years from now.

1. Identify key features - the design goals roughly spell out key features right
now but I know there are a few more rattling in my head.
2. Design the command line interface - I'd like the data model to reflect how the
tool is used and what users need, and not the inverse. When I designed Perforce
streams I effectively wrote `p4 help` for each command and showed the user workflow
before a line of code was written. It worked then, so why not now?
3. Design all the requisite data models for the core functionality to try to minimize
surprises.
3. Identify the minimal set of features required to get to self-hosting status.
4. Just keep going till it's done.

So yeah that's the plan. Will it turn into real software? Who knows. I do know
that I spent 16 years of my life building and designing various parts of Perforce,
and the happiest days of my career were spent building software that made other
people's lives easier, so I suspect once I get this thing going it will keep
going.

All design docs, data model designs, and sketches will be posted here. Feel free
to contribute or just point and laugh. Either seems legit at this point.

Strap in y'all; I think we're in for some fun.