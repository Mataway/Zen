No project is an island; along the way there are many blog posts, SO
answers, and bits of sample code that provide knowledge and guidance.
This file thanks the people and projects who helped, even if
inadvertently.

## How to layout a Go project
https://github.com/golang-standards/project-layout
https://medium.com/golang-learn/go-project-layout-e5213cdcfaa2

## Getting started with Go help
http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/


## Advice on handling contributors gracefully
https://github.com/kentcdodds/all-contributors
